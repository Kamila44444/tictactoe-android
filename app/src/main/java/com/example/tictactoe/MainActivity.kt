package com.example.tictactoe

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.util.TypedValue


class MainActivity : AppCompatActivity() {
    var botWorking = false
    var players: ArrayList<String> = ArrayList()
    var moveArray = intArrayOf(0 , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preparePlayersList()
        prepareButtons()
    }
    private fun prepareButtons() {
        for(i in (1..25)){
            val button = findViewById<Button>(getButtonId("button$i"))
            val height = windowManager.defaultDisplay.height

            var value = 6
            if(height > 800)
                value = 25
            button.setTextSize (TypedValue.COMPLEX_UNIT_SP,
                value.toFloat())
            Log.d("myDebug", "${button.textSize} ${height.toFloat()}")
        }
    }
    private fun preparePlayersList() {
        val array = resources.getStringArray(R.array.players)
        for (w in array) {
            players.add(w)
        }
    }

    private fun getButtonId(resName: String): Int {

        return try {
            return resources.getIdentifier(resName, "id", packageName)
        } catch (e: Exception) {
            e.printStackTrace()
            -1
        }

    }

    fun attak(v: View) {
        botWorking = switch1.isChecked
        val button = findViewById<Button>(v.id)
        if (button.text.isEmpty()) {
            if (playerText.text == "Player1") {
                button.text = "X"
                checkScore()
                if (botWorking) {
                    botMove()
                }
            } else {
                button.text = "O"
                checkScore()
            }
        }
    }

    private fun checkScore() {
        for (i in (1..5)) {
            val fnr = i
            val fbutton = findViewById<Button>(getButtonId("button$fnr"))
            if(!fbutton.text.isEmpty()) {
                if (checkVertical(fnr, fbutton) || checkHorizontal(fnr, fbutton)) {
                    win()
                    return
                }
            }
        }
        if (checkDiagonal(25, intArrayOf(19, 13, 7, 1)) || checkDiagonal(5, intArrayOf(9, 13, 17, 21))) {
            win()
            return
        }
        if (checkRemis()) {
            remis()
            return
        }
        changePlayer()
    }

    private fun checkRemis(): Boolean {
        var inRow = true
        for(i in (1..25)){
            val button = findViewById<Button>(getButtonId("button$i"))
            if(button.text.isEmpty()) {
                inRow = false
                break
            }
        }
        return inRow
    }

    private fun checkDiagonal(nr: Int, tmp: IntArray): Boolean {
        var inRow = true
        val sbutton = findViewById<Button>(getButtonId("button$nr"))
        for(i in tmp){
            val button = findViewById<Button>(getButtonId("button$i"))
            if(sbutton.text.toString() != button.text.toString() || sbutton.text.isEmpty()) {
                inRow = false
                break
            }
        }

        Log.d("myDebug2", "Diagonal $nr")
        return inRow
    }

    private fun checkVertical(fnr: Int, fbutton: Button?): Boolean {
        var inRow = true
        for (j in (1..4)) {
            val nr = (j * 5) + fnr
            val nbutton = findViewById<Button>(getButtonId("button$nr"))
            Log.d("myDebug", "$fnr")
            Log.d("myDebug", nbutton.text.toString())
            if (fbutton!!.text != nbutton.text) {
                inRow = false
                break
            }
        }
        Log.d("myDebug2", "Vertical $fnr")
        return inRow
    }

    private fun checkHorizontal(fnr: Int, fbutton: Button?): Boolean {
        var inRow = true
        for (j in (1..4)) {
            val nr = fnr + j
            val nbutton = findViewById<Button>(getButtonId("button$nr"))
            if (fbutton!!.text != nbutton.text) {
                inRow = false
                break
            }
        }
        Log.d("myDebug2", "Horizontal $fnr")
        return inRow
    }

    private fun changePlayer() {
        if (playerText.text == players[0]) {
            playerText.text = players[1]
        } else
            playerText.text = players[0]
    }

    private fun win() {
        val str = "Wygrał " + playerText.text
        Toast.makeText(applicationContext, str, Toast.LENGTH_SHORT).show()
        changePlayer()
        newGame()
    }
    private fun remis() {
        val str = "Remis"
        Toast.makeText(applicationContext, str, Toast.LENGTH_SHORT).show()
        changePlayer()
        newGame()
    }

    private fun newGame() {
        botWorking = switch1.isActivated
        moveArray = intArrayOf(0 , 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
        for(i in (1..25)){
            findViewById<Button>(getButtonId("button$i")).text = ""
        }
        playerText.text = players[0]
        Toast.makeText(applicationContext, "Nowa gra", Toast.LENGTH_SHORT).show()
    }

    private fun botMove() {
        for(i in (1..25)){
            if(moveArray[i] == 0) continue
            val button = findViewById<Button>(getButtonId("button$i"))
            if(!button.text.toString().isEmpty()){
                moveArray[i] = 0
                continue
            }

            val moveArray1 = moveArray.clone()
            val moveArray2 = moveArray.clone()
            val moveArray3 = moveArray.clone()
            val moveArray4 = moveArray.clone()

            moveArray = checkVerticalBot(moveArray1, i).clone()
            showArray(moveArray1, "1")
            sumToMoves(checkHorizontalBot(moveArray2, i))
            showArray(moveArray2, "2")
            sumToMoves(checkDiagonalBot(moveArray3, i, intArrayOf(25,19, 13, 7, 1)))
            showArray(moveArray3, "3")
            sumToMoves(checkDiagonalBot(moveArray4, i, intArrayOf(5, 9, 13, 17, 21)))
            showArray(moveArray4, "4")
            showArray(moveArray, "g")

        }
        val nr = find(moveArray.max())
        val button = findViewById<Button>(getButtonId("button$nr"))
        button.text = "O"
        clearMoves()
        checkScore()
    }

    private fun checkDiagonalBot(array: IntArray, i: Int, numbers: IntArray): IntArray {
        if(numbers.contains(i)){
            for(j in numbers){
                if(checkChance(array, j, i) == 0) break
            }
        }
        return array
    }

    private fun checkHorizontalBot(array: IntArray, i: Int): IntArray {
        for(j in (0..4)){
            var nr = ((i/5)*5+5 - j)
            if(i%5 == 0) nr -= 5
            if(checkChance(array, nr, i) == 0) break
        }
        return array
    }

    private fun checkVerticalBot(array: IntArray, i: Int): IntArray {
        for(j in (0..4)){
            var nr = i%5 + 5*j
            if(i%5 == 0) nr += 5
            if(checkChance(array, nr, i) == 0) break
        }
        return array
    }

    private fun checkChance(array: IntArray, nr: Int, i: Int): Any {
        if(i == (nr)) return 1
        val jbutton = findViewById<Button>(getButtonId("button$nr"))
        if(jbutton.text.toString() == "O"){
            if(array[i] == 1 ) array[i] += 3
            else if(array[i]%2 == 0) array[i] += 2
            else {
                array[i] = 1
                return 0 }
        }
        if(jbutton.text.toString() == "X"){
            if(array[i]%2 == 1) array[i] += 2
            else {
                array[i] = 1
                return 0}
        }
        return 1
    }

    private fun sumToMoves(moveArray1: IntArray) {
        for(i in (1..25)){
            if(moveArray1[i] == 0 || moveArray[i] == 0){
                moveArray[i] = 0
                continue
            }
            if(moveArray1[i] > moveArray[i] ){
                moveArray[i] = moveArray1[i]
            }
        }
    }

    private fun clearMoves() {
        for(i in (1..25)){
            if(moveArray[i] == 0) continue
            moveArray[i] = 1
        }
    }

    private fun showArray(moveArray: IntArray, tag : String) {
        var t = -1
        var str =""
        for(i in moveArray){
            t++
            str += "($t, $i)"
        }
        Log.d("botDebug", tag + str )

    }

    private fun find(predicate: Int?): Int? {
        Log.d("botDebug", "predict " + predicate.toString())
        for(i in (1..25)){
            if(moveArray[i] == predicate){
                Log.d("botDebug", "i $i" )
                return i
            }
        }
        return 1
    }

}

