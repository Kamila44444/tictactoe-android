package com.example.tictactoe

import android.content.Context
import android.util.AttributeSet
import android.widget.TableLayout

class MyLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : TableLayout(context, attrs){

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val size: Int
        if (widthMode == MeasureSpec.EXACTLY && widthSize > 0) {
            size = widthSize
        } else if (heightMode == MeasureSpec.EXACTLY && heightSize > 0) {
            size = heightSize
        } else {
            size = if (widthSize < heightSize) widthSize else heightSize
        }

        val finalMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY)
        super.onMeasure(finalMeasureSpec, finalMeasureSpec)
    }

}